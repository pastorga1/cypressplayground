import { urls } from '/Users/adminpa/Documents/testing-playground-master/cypress/support/urls.js';
import { login } from '/Users/adminpa/Documents/testing-playground-master/cypress/selectors/pages/authentication/login.js';
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add("login", (email, password) => { 
    cy.visit(urls.login);
    cy.get(login.userInput).type(email);
    cy.get(login.passwordInput).type(password);
    cy.get(login.loginButton).click();

});
