/// <reference types="Cypress" />

import { urls } from '/Users/adminpa/Documents/testing-playground-master/cypress/support/urls.js';
import { login } from '/Users/adminpa/Documents/testing-playground-master/cypress/selectors/pages/authentication/login.js';
import { myAccount } from '/Users/adminpa/Documents/testing-playground-master/cypress/selectors/pages/myAccount/myAccount.js';

describe('Testing log in page', () => {
    let email;

    it('should able to log in correctly via UI', () => {
        cy.visit(urls.login);
        cy.get(login.userInput).type(Cypress.env('QA_ADMIN'));
        cy.get(login.passwordInput).type(Cypress.env('QA_PASSWORD'));
        cy.get(login.loginButton).click();
        cy.title().should('eq', 'My account – Testing Playground');
        cy.get(myAccount.accountSection).within(() => {
            cy.contains('My account').should('be.visible');
            email = Cypress.env('QA_ADMIN');
            //const name = email.substring(0, email.lastIndexOf("@"));
            const name = Cypress.env('QA_ADMIN');
            cy.contains('Hello ' + name + ' (not ' + name + '? Log out)').should('be.visible');
        });
    });




});
