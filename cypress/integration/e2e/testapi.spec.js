/// <reference types="Cypress" />

import { endpoints } from '/Users/adminpa/Documents/testing-playground-master/cypress/support/endpoints.js';

let newProduct = {
  'name': 'New product 8',
  'type': 'simple',
  'regular_price': '21.99',
  'description': 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.',
  'short_description': 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
  'categories': [{ 'id': 9 }, { 'id': 14 }],
};

let invalidProduct = {
  'type': 'simple',
  'description': 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.',
  'short_description': 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
};

let updatedProduct = {
  'name': 'New product name',
  'regular_price': '10.99',
};

let productId;

describe('Testing api product CRUD actions', () => {
  it('should create a new product', () => {

    cy.request({
      method: 'POST',
      url: endpoints.products,
      auth: {
        username: 'automation',
        password: 'automation'
      },
      body: newProduct
    }).as('create_product');

    cy.get('@create_product').should((response) => {
      expect(response.status).to.eq(201);
      expect(response.body.name).to.eq('New product 8');
      expect(response.body.regular_price).to.eq('21.99');
      productId = response.body.id;
    });
  });

  it('should update created product', () => {
    cy.request({
      method: "PUT",
      url: endpoints.products + '/' + productId,
      auth: {
        username: 'automation',
        password: 'automation'
      },
      body: updatedProduct,
    }).as('update_product');

    cy.get('@update_product').should((response) => {
      expect(response.status).to.eq(200);
      expect(response.body.name).to.eq('New product name');
      expect(response.body.regular_price).to.eq('10.99');
    });
  });

  it('should delete created product', () => {
    cy.request({
      method: "DELETE",
      url: endpoints.products + '/' + productId + '?force=true',
      auth: {
        username: 'automation',
        password: 'automation'
      },
    }).as('delete_product');

    cy.get('@delete_product').should((response) => {
      expect(response.status).to.eq(200);
    });
  });

  it('should deleted product not be found', () => {
    cy.request({
      method: "GET",
      url: endpoints.products + '/' + productId,
      auth: {
        username: 'automation',
        password: 'automation'
      },
      failOnStatusCode: false,
    }).as('get_product');

    cy.get('@get_product').should((response) => {
      expect(response.body.data.status).to.eq(404);
      expect(response.body.message).to.eq('Invalid ID.');
    });
  });
});
