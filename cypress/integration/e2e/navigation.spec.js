/// <reference types="Cypress" />

import { commonSelectors } from '/Users/adminpa/Documents/testing-playground-master/cypress/selectors/pages/commonSelectors/commonSelectors.js';
import { home } from '/Users/adminpa/Documents/testing-playground-master/cypress/selectors/pages/homePage/home.js';
import { shopPage } from '/Users/adminpa/Documents/testing-playground-master/cypress/selectors/pages/shopPage/shopPage.js';



describe('Testing navigation', () => {

    beforeEach(() => {
        cy.login(Cypress.env('QA_ADMIN'), Cypress.env('QA_PASSWORD'));
    })

    it('should able to navigate to the home page', () => {
        cy.get(commonSelectors.navigationbar).within(() => {
            cy.contains('Home').click();

        });
        cy.title().should('eq', 'Testing Playground – This is a real-world app built with wordpress to be used as a playground environment');
        cy.get(commonSelectors.navigationbar).within(() => {
            cy.contains('Home').should('have.attr', 'aria-current', 'page');
        });
        cy.get(home.primaryPage).within(() => {
            cy.contains('Personal Styling for Everybody').should('be.visible');
            cy.contains('Shop by Category').should('be.visible');
            cy.contains('Shop by Category').should('be.visible');
            cy.contains('New In').should('be.visible');
            cy.contains('We Recommend').should('be.visible');
            cy.contains('Fan Favorites').should('be.visible');
            cy.contains('On Sale').should('be.visible');
            cy.contains('Best Sellers').should('be.visible');
        });
    });

    it('should able to navigate to the shop page', () => {
        cy.get(commonSelectors.navigationbar).within(() => {
            cy.contains('Shop').click();
        });
        cy.title().should('eq', 'Products – Testing Playground');
        cy.get(commonSelectors.navigationbar).within(() => {
            cy.contains('Shop').should('have.attr', 'aria-current', 'page');
        });
        cy.get(shopPage.pageContent).within(() => {
            cy.contains('Shop').should('be.visible');
        });
    });

    it('should able to navigate to the cart page from the navigation bar', () => {
        //cy.scrollTo('top')
        //cy.get(commonSelectors.navigationbar).within(() => {
        //cy.get(commonSelectors.shopBtn).trigger('mouseover');
        //});

        cy.scrollTo('top');
        cy.contains('li', 'Shop').trigger('focus');
        //cy.wait(2000);
        cy.get('[class="sub-menu"]').contains('Cart').click();
        //cy.get(commonSelectors.navigationbar).contains('Shop').trigger('mouseover');
        //cy.get('[class="sub-menu"]').invoke('show').find(commonSelectors.cartBtn).click();
        //cy.get(commonSelectors.cartBtn).click();
        cy.title().should('eq', 'Cart – Testing Playground');
        // cy.get(commonSelectors.navigationbar).within(() => {
        //     cy.contains('Shop').should('have.attr','aria-current', 'page');
        // });
        // cy.get(shopPage.pageContent).within(() =>{
        //     cy.contains('Shop').should('be.visible');
        // });
    });

    it('should able to navigate to the account details page from the navigation bar', () => {
        cy.get(commonSelectors.navigationbar).within(() => {
            cy.get(commonSelectors.accountBtn).click();
        });
        cy.title().should('eq', 'My account – Testing Playground');
    });

});
