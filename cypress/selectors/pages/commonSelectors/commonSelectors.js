export const commonSelectors = {
    navigationbar: '.primary-navigation',
    homeBtn: '[href="http://localhost:8000/"]',
    shopBtn: '[href="http://localhost:8000/shop/"]',
    cartBtn: '.menu-item [href="http://localhost:8000/cart/"]',
    accountBtn: '[href="http://localhost:8000/my-account/edit-account/]"',
  };