export const login = {
    loginButton: '[name="login"]',
    userInput: '[id="username"]',
    passwordInput: '[id="password"]',
  };